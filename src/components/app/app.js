import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from '../dashboard';
import Support from '../support';
import './app.scss'

export default class App extends Component {

    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/" exact render={() => (
                        <Dashboard pageTitle={'Dashboard'} />
                    )} />

                    <Route path="/support" exact render={() => (
                        <Support pageTitle={'Support'} />
                    )} />
                </Switch>
            </Router>
        )
    }
}