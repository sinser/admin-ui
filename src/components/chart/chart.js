import React, { Component } from 'react'
import { LineChart, Line, CartesianGrid, XAxis, YAxis } from 'recharts';


import './chart.scss'

export default class Chart extends Component {

    state = {
        chartData: [],
    }

    componentWillMount() {
        const { chartData } = this.props
        this.setState({ chartData })
    }

    componentDidUpdate(prevProps) {
        const { chartData } = this.props
        if (chartData !== prevProps.chartData) {
            this.setState({ chartData });
        }
    }

    getArrayItems = (arr) => {
        const data = arr.map((item) => {
            return { name: item.x, uv: item.y }
        })
        return data
    }

    renderChart = (chartData) => {
        const data = this.getArrayItems(chartData)
        return (
            <LineChart width={700} height={300} data={data}>
                <Line type="monotone" dataKey="uv" stroke="#8884d8" />
                <CartesianGrid stroke="#ccc" />
                <XAxis dataKey="name" />
                <YAxis />
            </LineChart>
        )
    }

    render() {
        const { chartData } = this.state
        return (
            <div className="row">
                <div className="col-md-12">
                    {this.renderChart(chartData)}
                </div>
            </div>
        )
    }
}