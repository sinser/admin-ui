import React, { Component } from 'react'
import SideBar from '../sidebar'
import Chart from '../chart'
import TableInfo from '../table-info'
import Footer from '../footer'
import PercentInfo from '../percent-info'
import PageTitle from '../../helpers/page-title'
import Input from '../../helpers/input'
import Button from '../../helpers/button'
import moment from 'moment'
import axiosFetch from '../../helpers/axios';

import { GET_TABLE_DATA, GET_PAGES } from '../../helpers/constants'

import './dashboard.scss'

export default class Dashboard extends Component {
    state = {
        dateFrom: '',
        dateTo: '',
        chartData: [],
        tableData: [],
        totalUtilizatori: 0,
        tablesHeaders: [
            { th: 'Data' },
            { th: 'Numar utilizator total' },
            { th: 'Numar utilizator unic' },
            { th: 'Numar utilizator repeta' },
            { th: 'Page' },
        ],
        tableArray: [],
        pagesArray: [],
    }

    componentWillMount() {
        this.getPages()
    }

    changeFieldsState = (field, inputValue) => {
        this.setState({ [field]: inputValue });
    }

    getTableData = () => {
        axiosFetch(GET_TABLE_DATA).then((tableArray) => {
            const startDate = "2020-09-01"
            const today = moment().format("YYYY-MM-DD");
            this.showChart(startDate, today, tableArray)
            this.setState({ tableArray });
        });
    }

    getPages = () => {
        axiosFetch(GET_PAGES).then((pagesArray) => {
            this.setState({ pagesArray });
            this.getTableData()
        });
    }

    showChart = (dateFrom, dateTo, tableArray) => {
        const { pagesArray } = this.state

        const data = tableArray.map((arrItem) => {
            const { date, userUnic, userRepeat, pageId } = arrItem
            if (moment(date).isAfter(dateFrom) && moment(date).isBefore(dateTo)) {
                return { date, userUnic, userRepeat, pageId }
            }
            return true
        })

        const filteredData = data.filter(item => item !== true);
        let chartData = []
        let tableData = []
        let totalUtilizatori = 0

        pagesArray.map((pageItem) => {
            const { name, id } = pageItem
            let countUsers = 0
            filteredData.forEach((item) => {
                const { date, pageId, userUnic, userRepeat } = item

                if (pageId === id) {
                    tableData.push({ date, pageId, name, userUnic, userRepeat })
                    countUsers += userUnic + userRepeat
                    totalUtilizatori += countUsers
                }
            })

            if (countUsers !== 0) {
                chartData.push({ x: name, y: countUsers })
            }
            return chartData
        })

        this.setState({ chartData, tableData, totalUtilizatori });
        return filteredData;
    }

    render() {
        const { pageTitle } = this.props
        const { dateFrom, dateTo, chartData, tablesHeaders, tableData, totalUtilizatori, tableArray } = this.state

        const disabled = dateFrom.length === 0 || dateTo.length === 0

        return (
            <div className="wrapper ">
                <SideBar activeMenuItem={pageTitle} />
                <div className="main-panel">
                    <PageTitle title={pageTitle} />
                    <div className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-3 date-container">
                                    <PercentInfo totalUtilizatori={totalUtilizatori} />
                                    <div className="card card-stats row">
                                        <div className="col-md-6">
                                            <label htmlFor="dateFrom">Date from</label>
                                            <Input className={"form-control"}
                                                type={"date"}
                                                id={"dateFrom"}
                                                onChange={(e) => this.changeFieldsState("dateFrom", e.target.value)}
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="dateFrom">Date to</label>
                                            <Input className={"form-control"}
                                                type={"date"}
                                                id={"dateTo"}
                                                onChange={(e) => this.changeFieldsState("dateTo", e.target.value)}
                                            />
                                        </div>
                                        <Button type={'button'}
                                            text={'Show result'}
                                            disabled={disabled}
                                            className={'btn btn-success waves-effect waves-light m-l-5'}
                                            onClick={() => this.showChart(dateFrom, dateTo, tableArray)} />
                                    </div>
                                </div>
                                <div className="col-md-9">
                                    <div className="chart">
                                        <Chart chartData={chartData} />
                                    </div>
                                </div>
                            </div>
                            <TableInfo tablesHeaders={tablesHeaders} tableData={tableData} />
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        )
    }
}