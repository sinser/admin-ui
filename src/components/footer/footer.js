import React, { Component } from 'react'
import './footer.scss'

export default class Footer extends Component {

    render() {

        return (
            <footer className="footer">
                <div className="container-fluid">
                    <div className="copyright float-right">
                        &copy;
                    test issue powered by <i className="material-icons"></i> by
                    <span className="nav-link">
                            Serghei Sinchin
                    </span>

                    </div>
                </div>
            </footer>
        )
    }
}