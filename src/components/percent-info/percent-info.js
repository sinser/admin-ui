import React, { Component } from 'react'

import './percent-info.scss'

export default class Footer extends Component {
    
    state = {
        totalUtilizatori: 0,
    }

    componentDidMount() {
        const { totalUtilizatori } = this.props
        this.setState({ totalUtilizatori });
    }

    componentDidUpdate(prevProps) {
        const { totalUtilizatori } = this.props
        if (totalUtilizatori !== prevProps.totalUtilizatori) {
            this.setState({ totalUtilizatori });
        }
    }

    render() {
        const { totalUtilizatori } = this.state
        return (
                <div className="card card-stats">
                    <div className="card-header card-header-warning card-header-icon">
                        <div className="card-icon">
                            <i className="material-icons">content_copy</i>
                        </div>
                        <p className="card-category">Instalari pe disposive active</p>
                        <h3 className="card-title">{totalUtilizatori}</h3>
                    </div>
                </div>
        )
    }
}