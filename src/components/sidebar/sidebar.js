import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import './sidebar.scss'

export default class SideBar extends Component {

    render() {
        const { activeMenuItem } = this.props
        return (
            <div className="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
                <div className="logo">
                    <span className="simple-text logo-normal">
                        Creative Tim
                    </span>
                </div>
                <div className="sidebar-wrapper">
                    <ul className="nav">
                        <li  className={classnames({
                            'nav-item': true,
                            'active': activeMenuItem === 'Dashboard',
                        })}>
                            <Link to={'/'} className="nav-link">
                                <i className="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </Link>
                        </li>
                        <li className={classnames({
                            'nav-item': true,
                            'active': activeMenuItem === 'Support',
                        })}>
                            <Link to={'/support'} className="nav-link">
                                <i className="material-icons">content_paste</i>
                                <p>Support</p>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}