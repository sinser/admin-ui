import React, { Component } from 'react'
import SideBar from '../sidebar'
import Footer from '../footer'
import SupportTable from '../support/support-table'
import PageTitle from '../../helpers/page-title'
import Input from '../../helpers/input'
import Button from '../../helpers/button'
import { GET_UTILIZATORI } from '../../helpers/constants'
import moment from 'moment'
import axiosFetch from '../../helpers/axios';

import './support.scss'

export default class Dashboard extends Component {
    state = {
        idnp: '',
        phone: '',
        data: '',
        tablesHeaders: [
            { th: 'Nume Prenume' },
            { th: 'IDNP' },
            { th: 'Nr. Telefon' },
            { th: 'PAN Card' },
            { th: 'Last Transaction' },
        ],
        tableData: [],
        utilizatoriArray: [],
    }

    componentDidMount() {
        this.getUtilizatori()
    }

    getUtilizatori = () => {
        axiosFetch(GET_UTILIZATORI).then((utilizatoriArray) => {
            this.setState({ utilizatoriArray });
            this.showUsersInfo(utilizatoriArray)
        });
    }

    changeFieldsState = (field, inputValue) => {
        this.setState({ [field]: inputValue });
    }

    showUsersInfo = (utilizatoriArray, idnp = "", phone = "", data = "") => {

        let result = utilizatoriArray

        if (idnp.length > 0) {
            result = result.filter((item) => {
                return item.idnp.toLowerCase().indexOf(idnp.toLowerCase()) > -1;
            });
        }
        if (phone.length > 0) {
            result = result.filter((item) => {
                return item.phone.toLowerCase().indexOf(phone.toLowerCase()) > -1;
            });
        }

        if (data.length > 0) {
            const newDateFormat = moment(data).format("YYYY-MM-DD");
            result = result.filter((item) => {
                return item.transactionDate.indexOf(newDateFormat) > -1;
            });
        }

        this.setState({ tableData: result })
    }

    render() {
        const { pageTitle } = this.props
        const { idnp, phone, data, tablesHeaders, tableData, utilizatoriArray } = this.state
        return (
            <div className="wrapper ">
                <SideBar activeMenuItem={pageTitle} />
                <div className="main-panel">
                    <PageTitle title={pageTitle} />
                    <div className="content">
                        <div className="container-fluid">
                            <div className="col-md-12 date-container">
                                <div className="card card-stats row">
                                    <div className="col-md-4">
                                        <label htmlFor="idnp">IDNP</label>
                                        <Input className={"form-control"}
                                            type={"text"}
                                            id={"idnp"}
                                            value={idnp}
                                            onChange={(e) => this.changeFieldsState("idnp", e.target.value)}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <label htmlFor="phone">Numar telefon</label>
                                        <Input className={"form-control"}
                                            type={"text"}
                                            id={"phone"}
                                            value={phone}
                                            onChange={(e) => this.changeFieldsState("phone", e.target.value)}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <label htmlFor="date">Data</label>
                                        <Input className={"form-control"}
                                            type={"date"}
                                            id={"date"}
                                            value={data}
                                            onChange={(e) => this.changeFieldsState("data", e.target.value)}
                                        />
                                    </div>
                                    <Button type={'button'}
                                        text={'Show result'}
                                        className={'btn btn-success waves-effect waves-light m-l-5'}
                                        onClick={() => this.showUsersInfo(utilizatoriArray, idnp, phone, data)} />
                                </div>
                            </div>

                            <SupportTable tablesHeaders={tablesHeaders} tableData={tableData} />
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        )
    }
}