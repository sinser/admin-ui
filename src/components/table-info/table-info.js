import React, { Component } from 'react'
import Spinner from '../../helpers/spinner'

import './table-info.scss'

export default class TableInfo extends Component {

    state = {
        tablesHeaders: [],
        tableData: [],
        showSpinner: true,
    }

    componentDidMount() {
        const { tablesHeaders, tableData } = this.props
        this.setState({ tablesHeaders, tableData, showSpinner: false });
    }

    componentDidUpdate(prevProps) {
        const { tablesHeaders, tableData } = this.props
        if (tableData !== prevProps.tableData) {
            this.setState({ tablesHeaders, tableData, showSpinner: true });
            setTimeout(() => {
                this.setState({ showSpinner: false });
            }, 500);
        }
    }

    renderTableHeaders = (items) => {
        const tableHead = items.map((item, i) => {
            const { th } = item
            return <th key={i}> {th} </th>
        })
        return tableHead
    }

    renderTableData = (items) => {
        const tableHead = items.map((item, i) => {
            const { date, name, userUnic, userRepeat } = item
            return (
                <tr key={i}>
                    <td> {date} </td>
                    <td> {userUnic + userRepeat} </td>
                    <td> {userUnic} </td>
                    <td> {userRepeat} </td>
                    <td> {name} </td>
                </tr>
            )
        })
        return tableHead
    }

    render() {
        const { tablesHeaders, tableData, showSpinner } = this.state

        return (
            <div className="row ">

                <div className="col-lg-12 col-md-12 table-content">
                    {showSpinner && <Spinner />}
                    <div className="card">
                        <div className="card-header card-header-warning">
                            <h4 className="card-title">Table Info</h4>
                        </div>
                        <div className="card-body table-responsive">
                            <table className="table table-hover">
                                <thead className="text-warning">
                                    <tr>
                                        {this.renderTableHeaders(tablesHeaders)}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderTableData(tableData)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}