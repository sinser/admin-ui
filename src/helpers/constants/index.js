export const API_URL = 'http://admin-ui.ssv-design.com/Main';

export const GET_TABLE_DATA = API_URL + '/getTableData';
export const GET_PAGES = API_URL + '/getPages';
export const GET_UTILIZATORI = API_URL + '/getUtilizatori';