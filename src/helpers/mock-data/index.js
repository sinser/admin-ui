//if page name will change - she will change everywhere
export const pageA = "Page A"
export const pageB = "Page B"
export const pageC = "Page C"
export const pageD = "Page D"
export const pageE = "Page E"
export const pageF = "Page F"
export const pageG = "Page G"
export const pageH = "Page H"

export const TABLE_MOCK_ARRAY = [

    { date: "2020-09-07", userUnic: 130, userRepeat: 890, page: pageH },
    { date: "2020-09-07", userUnic: 134, userRepeat: 143, page: pageC },
    { date: "2020-09-13", userUnic: 135, userRepeat: 175, page: pageE },
    { date: "2020-09-19", userUnic: 188, userRepeat: 145, page: pageE },
    { date: "2020-09-20", userUnic: 124, userRepeat: 273, page: pageF },
    { date: "2020-09-25", userUnic: 128, userRepeat: 278, page: pageC },
    { date: "2020-09-29", userUnic: 176, userRepeat: 245, page: pageA },

    { date: "2020-10-07", userUnic: 130, userRepeat: 891, page: pageH },
    { date: "2020-10-18", userUnic: 110, userRepeat: 332, page: pageA },
    { date: "2020-10-20", userUnic: 137, userRepeat: 455, page: pageD },
    { date: "2020-10-23", userUnic: 258, userRepeat: 128, page: pageB },
    { date: "2020-10-25", userUnic: 235, userRepeat: 327, page: pageA },

    { date: "2020-11-03", userUnic: 112, userRepeat: 104, page: pageC },
    { date: "2020-11-07", userUnic: 198, userRepeat: 178, page: pageC },
    { date: "2020-11-13", userUnic: 187, userRepeat: 145, page: pageE },
    { date: "2020-11-17", userUnic: 145, userRepeat: 865, page: pageH },
    { date: "2020-11-20", userUnic: 132, userRepeat: 256, page: pageF },
    { date: "2020-11-23", userUnic: 165, userRepeat: 238, page: pageF },
]

export const PAGES_MOCK_ARRAY = [
    { name: pageA },
    { name: pageB },
    { name: pageC },
    { name: pageD },
    { name: pageE },
    { name: pageF },
    { name: pageG },
    { name: pageH },
]

export const UTILIZATOR_MOCK_ARRAY = [
    { name: "Vasilii Terkin", idnp: "456453456", phone: "+373691265454", card: "1234567814271212", transactionDate: "2019-03-05" },
    { name: "Kostea Popov", idnp: "456735354", phone: "+373694565465", card: "4556567845771212", transactionDate: "2020-12-15" },
    { name: "Dmitrii Derevtsev", idnp: "78645343", phone: "+373693265443", card: "7852058872377863", transactionDate: "2020-10-15" },
    { name: "Evgenii Kudreshov", idnp: "4826482343", phone: "+373764565465", card: "12345678782121212", transactionDate: "2019-10-25" },
    { name: "Ibragim Ceban", idnp: "45642682", phone: "+373694365465", card: "1234277842241212", transactionDate: "2020-10-13" },
    { name: "Svetlan Katalov", idnp: "2363473543", phone: "+373454565423", card: "1234567895681212", transactionDate: "2018-08-15" },
    { name: "Avgust Durakov", idnp: "3245624367", phone: "+373695465465", card: "2557535895681212", transactionDate: "2020-09-14" },
    { name: "Denis Perilov", idnp: "3132454375", phone: "+373344534565", card: "12345678532045586", transactionDate: "2019-10-15" },
    { name: "Stepan Sidorov", idnp: "4356257246", phone: "+373234523465", card: "6567567848325657", transactionDate: "2020-07-15" },
]