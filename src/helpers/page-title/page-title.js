import React, { Component } from 'react';

export default class PageTitle extends Component {
    render() {
        const { title, customClass = 'navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top' } = this.props;
        return (
            <nav className={customClass}>
                <div className="container-fluid">
                    <div className="navbar-wrapper">
                        <span className="navbar-brand">{title}</span>
                    </div>
                </div>
            </nav>
        )
    };
}

